<?php
	// cek $_GET
	if (!isset($_GET["gambar"]) ||
		!isset($_GET["merek"]) ||
		!isset($_GET["type"]) ||
		!isset($_GET["warna"]) ||
		!isset($_GET["harga"])) {
		header("Location:latihan1.php");
		exit;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>GET</title>
	<style type="text/css" media="screen">
		.kotak{
			width: 250px;
			min-height: 260px;
			background-color: whitesmoke;
			border: 2px solid #999;
			font-family: arial;
			font-weight: bold;
			border-radius:  4px;
			transition: 1s;
			float: left;
			margin-right: 10px;
			margin-bottom: 10px;
		}
		.kotak:hover{
			background-color: cyan;
			color:  black;
		}
		.kotak img{
			box-shadow: 0px 0px 10px black;
			width: 150px;
			line-height: 170px;
			height: 170px;
		}
		.clear{
			clear: both;
			margin-bottom: 10px;
		}
		ul{
			list-style: none;
		}
	</style>
</head>
<body>
	<div class="kotak">
		<ul>
			<li><img src="img/<?= $_GET["gambar"]; ?>" alt="gambar"></li>
			<li>Merek : <?= $_GET["merek"]; ?></li>
			<li>Type : <?= $_GET["type"]; ?></li>
			<li>Warna : <?= $_GET["warna"]; ?></li>
			<li>Harga : <?= $_GET["harga"]; ?></li>
		</ul>
	</div>
	<div class="clear"></div>
	<a href="latihan1.php" title="kembali">Kembali ke daftar lemari</a>
</body>
</html>