<!-- Materi latihan $_GET $_POST -->

<!DOCTYPE html>
<html>
<head>
	<title>GET dan POST</title>
	<style type="text/css" media="screen">
		.kotak{
			width: 250px;
			min-height: 260px;
			background-color: whitesmoke;
			border: 2px solid #999;
			font-family: arial;
			font-weight: bold;
			border-radius:  4px;
			transition: 1s;
			float: left;
			margin-right: 10px;
			margin-bottom: 10px;
		}
		.kotak:hover{
			background-color: cyan;
			color:  black;
		}
		.kotak img{
			box-shadow: 0px 0px 10px black;
			width: 150px;
			line-height: 170px;
			height: 170px;
		}
		.clear{
			clear: both;
			margin-bottom: 10px;
		}
		ul{
			list-style: none;
		}
	</style>
</head>
<body>

<?php
$lemari = [
	[
		"gambar" => "MEJA-BELAJAR-2.jpg",
		"merek" => "Olympic",
		"type" => "SM-01",
		"warna" => "Coklat",
		"harga" => "Rp. 3.000.000,00",
	],
	[
		"gambar" => "2763green_sport.jpg",
		"merek" => "Napoly",
		"type" => "SM-02",
		"warna" => "Coklat",
		"harga" => "Rp. 4.000.000,00",
	],
	[
		"gambar" => "meja_belajar_olympic_model_terbaru.jpg",
		"merek" => "Jepara",
		"type" => "SM-03",
		"warna" => "Abu-abu",
		"harga" => "Rp. 5.000.000,00",
	],
	[
		"gambar" => "N-85-Jual-meja-Belajar-Anak1.jpg",
		"merek" => "Olympic",
		"type" => "SM-04",
		"warna" => "Coklat",
		"harga" => "Rp. 6.000.000,00",
	],
	[
		"gambar" => "Meja-Belajar-Anak-Warna-Hitam.jpg",
		"merek" => "Olympic",
		"type" => "SM-05",
		"warna" => "Hitam",
		"harga" => "Rp. 6.000.000,00",
	],
	[
		"gambar" => "gbr_meja-belajar-anak-sd_desainrumah.jpg",
		"merek" => "Olympic",
		"type" => "SM-06",
		"warna" => "Cokelat",
		"harga" => "Rp. 6.000.000,00",
	],
	[
		"gambar" => "meja_belajar_anak_model_minimalis_terbaru.jpg",
		"merek" => "Olympic",
		"type" => "SM-07",
		"warna" => "Hitam",
		"harga" => "Rp. 6.000.000,00",
	],
	[
		"gambar" => "Meja-Belajar-Minimalis-Rak-Buku.jpg",
		"merek" => "Olympic",
		"type" => "SM-08",
		"warna" => "Coklat",
		"harga" => "Rp. 6.000.000,00",
	],
];
?>

<?php foreach ($lemari as $lm) : ?>
	<div class="kotak">
		<ul>
			<li>
				<a href="latihan2.php?gambar=<?= $lm["gambar"]; ?>&merek=<?= $lm["merek"]; ?>&type=<?= $lm["type"]; ?>&warna=<?= $lm["warna"]; ?>&harga=<?= $lm["harga"]; ?>"><img src="img/<?= $lm["gambar"]; ?>" alt="gambar"></a>
			</li>
		</ul>
	</div>
<?php endforeach; ?>

</body>
</html>