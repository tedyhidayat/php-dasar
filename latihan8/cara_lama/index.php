<?php
//koneksi database
	$conn = mysqli_connect("localhost","root","","db_latihan");

//ambil data dari table
	$sql = mysqli_query($conn, "SELECT * FROM tbl_furniture");
	// while ($data = mysqli_fetch_assoc($sql)) {
	//     var_dump($data);
	// }

?>





<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Latihan php crud 1</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" />
    <script src="bootstrap/js/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
	<div class="row text-center ">
		<h1>Index Admin</h1>
	</div>
	<div class="row">
		<h3>Data Furniture</h3>
		<table class="table table-hover table-bordered">
		    <thead>
		      <tr class="info">
		      	<th>No.</th>
		        <th>Gambar</th>
		        <th>Merek</th>
		        <th>Type</th>
		        <th>Warna</th>
		        <th>Harga</th>
		        <th class="text-center">Aksi</th>
		      </tr>
		    </thead>
		    <tbody>
		    	<?php $no = 1; ?>
		    <?php while( $data = mysqli_fetch_assoc($sql) ) : ?>
		      <tr>
		        <td><?= $no++; ?></td>
		        <td><img src="img/<?= $data["gambar"]; ?>" width="50 alt="furniture"></td>
		        <td><?= $data["merek"]; ?></td>
		        <td><?= $data["type"]; ?></td>
		        <td><?= $data["warna"]; ?></td>
		        <td><?= $data["harga"]; ?></td>
		        <td class="text-center">
		        	<a class="btn btn-primary btn-sm" href="">
		        		<i class="glyphicon glyphicon-edit"></i>
		        	</a>
					<a class="btn btn-danger btn-sm" href="">
						<i class="glyphicon glyphicon-trash"></i>
					</a>
		        </td>
		      </tr>
		     <?php endwhile; ?>
		    </tbody>
		  </table>
	</div>
</div>

<!-- script type="text/javascript">
$(function(){
    $('.back_top').on('click', function(){
        $('html').animate({scrollTop : 0}, 800);
    });
    $(window).on('scroll', function(){
        if($(this).scrollTop() > 100){
            $('.back_top').show();
        }
    });
});
</script> -->
</body>
</html>