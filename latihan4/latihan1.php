<?php
//array
//Variabel yang  dapat memiliki banyak nilai
//elemen pada array boleh memiliki tipe data yang berbeda
//pasangan array adalah key dan value
//key nya adalah index yang di mulai dari 0

//membuat array
// cara lama
// $hari = array("senin","selasa","rabu","kamis","jumat","sabtu","minggu");
// // cara baru
// $bulan = ["januari","februari","maret","april","mei","juni","juli"];
// //tipe data yang berbeda
// $tahun = [2010, "duaribu sebelas", true];

//cara menampilkan array dengan = var_dump, print_r, echo[]

//var_dump
// var_dump($hari);
// echo "<br>";

// //print_r
// print_r($bulan);
// echo "<br>";

// //echo[]
// echo $tahun[0];
// $bulan[] = "agustus";
// $bulan[] = "september";
// $bulan[] = "oktober";
// var_dump($bulan);

?>