<?php
//menampilkan array dengan versi user
//menggunakan for dan foreach(khusus array)

$angka = [1,2,3,4,5,6,7,8,9,10];

?>

<!DOCTYPE html>
<html>
<head>
	<title>Menampilkan array versi userside</title>
	<style type="text/css" media="screen">
		.kotak{
			width:50px;
			height: 50px;
			background-color: black;
			color:  white;
			text-align: center;
			line-height: 50px;
			float: left;
			margin-right: 5px;
		}
		.warna{
			background-color: gray;
			color: white;
		}
		.clear {clear: both; margin-top: 10px;}
	</style>
</head>
<body>
<!-- cara 1  -->
<?php for ($i = 0; $i < count($angka); $i++) { ?>
	<div class="kotak"><?php echo $angka[$i];?></div>
<?php } ?>

<div class="clear"></div>

<!-- cara 2  -->
<?php foreach ($angka as $number ) { ?>
	<div class="kotak"><?php echo $number;?></div>
<?php } ?>

<div class="clear"></div>

<!-- cara 3 (lebih rapih dengan gaya templating)  -->
<?php foreach ($angka as $a ) : ?>
	<div class="kotak"><?= $a;?></div>
<?php endforeach; ?>

<div class="clear"></div>

<!-- cara 3 modif (lebih rapih dengan gaya templating)  -->
<?php foreach ($angka as $a ) : ?>
	<?php if($angka[0]++ % 2 == 0) : ?>
		<div class="kotak warna"><?= $a;?></div>
	<?php else : ?>
		<div class="kotak"><?= $a;?></div>
	<?php endif; ?>
<?php endforeach; ?>


</body>
</html>