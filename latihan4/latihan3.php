<?php
// latihan array dinamis(araray di dalam array)

$data_siswa = [
	["Tedy Hidayat","XIRPL2","Rekayasa Perangkat Lunak","SMKN 2 KOTA BEKASI"],
	["Akbar Nurkholis Fajri","XIRPL1","Rekayasa Perangkat Lunak","SMKN 2 KOTA BEKASI"],
	["Muhammad Arya","XIRPL1","Rekayasa Perangkat Lunak","SMKN 2 KOTA BEKASI"],
	["Gilang Chandra","XIRPL1","Rekayasa Perangkat Lunak","SMKN 2 KOTA BEKASI"],
];
?>

<!DOCTYPE html>
<html>
<head>
	<title>Daftar Siswa</title>
	<style type="text/css" media="screen">
		ul{
			list-style: none;
			font-family: arial;
		}
	</style>
</head>
<body>

<h1>Data Siswa</h1>

<!-- <ul>
	<?php foreach ($data_siswa as $siswa) :?>
		<li><?= $siswa; ?></li>
	<?php endforeach; ?>
</ul> -->

<?php foreach ($data_siswa as $siswa) :?>
<ul>
	<li>Nama : <?= $siswa[0]; ?></li>
	<li>Kelas : <?= $siswa[1]; ?></li>
	<li>Jurusan : <?= $siswa[2]; ?></li>
	<li>Sekolah : <?= $siswa[3]; ?></li>
</ul>
<?php endforeach; ?>
</body>
</html>