<?php
require "functions.php";

//ambil data dari table menggunakan function
	$data_furniture = read("SELECT * FROM tbl_furniture");

// pencarian
	if ( isset($_POST['cari']) ) {
		$data_furniture = cari( $_POST['keyword'] );
	}
?>





<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Latihan php crud 1</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" />
    <script src="bootstrap/js/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container-fluid">
	<div class="row text-center">
		<h1>Index Admin</h1>
	</div>
	<div class="row">
		<div class="col-md-8">
			<h3 style="margin-bottom: 20px;">Data barang</h3>
			<form action="" method="post" class="form-inline" role="form">
				<div class="form-group" style="margin-bottom: 10px;">
				    <div class="form-group">
				      <input class="form-control input-sm" name="keyword" type="text" placeholder="Cari data" autofocus autocomplete="off">
				    </div>
				    <button class="btn btn-primary btn-sm" type="submit" name="cari"><i class="glyphicon glyphicon-search"></i></button>
				</div>
			</form>
		</div>
		<div class="col-md-4 text-right" style="margin-top: 65px;">
			<a class="btn btn-success btn-xs" href="tambah.php">Tambah data</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<table class="table table-hover table-bordered">
			    <thead>
			      <tr class="success">
			      	<th>No.</th>
			        <th>Gambar</th>
			        <th>Merek</th>
			        <th>Type</th>
			        <th>Warna</th>
			        <th>Harga</th>
			        <th class="text-center">Aksi</th>
			      </tr>
			    </thead>
			    <tbody>
			    <?php $no = 1; ?>
			    <?php foreach( $data_furniture as $data ) : ?>
			      <tr>
			        <td width="50" style="background-color: whitesmoke; color: black;"><?= $no++; ?>.</td>
			        <td><img src="img/<?= $data["gambar"]; ?>" width="50 alt="furniture"></td>
			        <td><?= $data["merek"]; ?></td>
			        <td><?= $data["type"]; ?></td>
			        <td><?= $data["warna"]; ?></td>
			        <td><?= $data["harga"]; ?></td>
			        <td class="text-center">
			        	<a class="btn btn-primary btn-md" href="edit.php?id=<?= $data['id']; ?>">
			        		<i class="glyphicon glyphicon-edit"></i>
			        	</a>
						<a class="btn btn-danger btn-md" href="hapus.php?id=<?= $data['id']; ?>" onclick="return confirm('Yakin ingin hapus data ?');">
							<i class="glyphicon glyphicon-trash"></i>
						</a>
			        </td>
			      </tr>
			     <?php endforeach; ?>
			    </tbody>
			  </table>
		</div>
	</div>
</div>

<!-- script type="text/javascript">
$(function(){
    $('.back_top').on('click', function(){
        $('html').animate({scrollTop : 0}, 800);
    });
    $(window).on('scroll', function(){
        if($(this).scrollTop() > 100){
            $('.back_top').show();
        }
    });
});
</script> -->
</body>
</html>