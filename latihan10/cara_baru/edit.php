<?php
require "functions.php";

$id = $_GET['id'];

$data = read("SELECT * FROM tbl_furniture WHERE id=$id")[0];

if ( isset($_POST["edit"]) ) {
	if (edit($_POST) > 0) {
		echo '
			<script type="text/javascript">
				alert("Data berhasil diedit !");
				document.location.href = "index.php";
			</script>
		';
	} else{
		echo '
			<script type="text/javascript">
				alert("Data gagal diedit !");
				document.location.href = "index.php";
			</script>
		';
	}
}

?>





<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Latihan php crud 1</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" />
    <script src="bootstrap/js/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="text-center">Ubah data</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-8">
			<form action="" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
			  <div class="form-group">
			    <div class="col-sm-11">
			    	<input type="hidden" name="id" class="form-control" placeholder="Masukan merek" required value="<?= $data['id']; ?>">
			    	<input type="hidden" name="gambarLama" class="form-control" placeholder="Masukan merek" required value="<?= $data['gambar']; ?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="merek" class="col-sm-1 control-label">Merek</label>
			    <div class="col-sm-11">
			    	<input type="text" name="merek" class="form-control" id="merek" placeholder="Masukan merek" required value="<?= $data['merek']; ?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="type" class="col-sm-1 control-label">Type</label>
			    <div class="col-sm-11">
			    	<input type="text" name="type" class="form-control" id="type" placeholder="Masukan type" required value="<?= $data['type']; ?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="warna" class="col-sm-1 control-label">Warna</label>
			    <div class="col-sm-11">
			    	<input type="text" name="warna" class="form-control" id="warna" placeholder="Masukan warna" required value="<?= $data['warna']; ?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="harga" class="col-sm-1 control-label">Harga</label>
			    <div class="col-sm-11">
			    	<input type="number" name="harga" class="form-control" id="harga" placeholder="Masukan harga" required value="<?= $data['harga']; ?>">
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="gambar" class="col-sm-1 control-label">Gambar</label>
			    <div class="col-sm-11">
			    	<input type="file" name="gambar" class="form-control" id="harga">
			    </div>
			  </div>
			  <div class="form-group">
				<div class="col-sm-offset-1 col-sm-11">
			  		<button type="submit" class="btn btn-primary" name="edit">Edit</button>
			  		<button type="reset" class="btn btn-danger">Reset</button>
			  	</div>
			  </div>
			</form>
		</div>
		<div class="col-sm-4 text-center">
			<img src="img/<?= $data['gambar']; ?>" alt="Gambar" class="img-thumbnail" width="250">
		</div>
	</div>
</div>
</body>
</html>