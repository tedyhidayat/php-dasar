<?php
require "functions.php";

if ( isset($_POST["tambah"]) ) {

	if (tambah($_POST) > 0) {
		echo "
			<script>
				alert('Data berhasil ditambahkan !');
				document.location.href = 'index.php';
			</script>
		";
	} else{
		echo "
			<script>
				alert('Data gagal ditambahkan !');
				document.location.href = 'index.php';
			</script>
		";
	}
}

?>





<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Latihan php crud 1</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="bootstrap/css/bootstrap.css" type="text/css" rel="stylesheet" />
    <script src="bootstrap/js/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h2 class="text-center">Tambah data</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2">
			<form action="" method="post" class="form-horizontal" role="form" enctype="multipart/form-data">
			  <div class="form-group">
			    <label for="merek" class="col-sm-1 control-label">Merek</label>
			    <div class="col-sm-11">
			    	<input type="text" name="merek" class="form-control" id="merek" placeholder="Masukan merek" required>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="type" class="col-sm-1 control-label">Type</label>
			    <div class="col-sm-11">
			    	<input type="text" name="type" class="form-control" id="type" placeholder="Masukan type" required>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="warna" class="col-sm-1 control-label">Warna</label>
			    <div class="col-sm-11">
			    	<input type="text" name="warna" class="form-control" id="warna" placeholder="Masukan warna" required>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="harga" class="col-sm-1 control-label">Harga</label>
			    <div class="col-sm-11">
			    	<input type="number" name="harga" class="form-control" id="harga" placeholder="Masukan harga" required>
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="gambar" class="col-sm-1 control-label">Gambar</label>
			    <div class="col-sm-11">
			    	<input type="file" name="gambar" class="form-control" id="harga" placeholder="Masukan gambar">
			    </div>
			  </div>
			  <div class="form-group">
				<div class="col-sm-offset-1 col-sm-11">
			  		<button type="submit" class="btn btn-success" name="tambah">Tambah</button>
			  		<button type="reset" class="btn btn-danger">Reset</button>
			  	</div>
			  </div>
			</form>
		</div>
	</div>
</div>
</body>
</html>