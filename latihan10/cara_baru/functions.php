<?php
//koneksi database
	$conn = mysqli_connect("localhost","root","","db_latihan");

	function read($query){
		global $conn;

		$sql = mysqli_query($conn, $query);
		$data = [];
		while ( $row = mysqli_fetch_assoc($sql) ){
		    $data[] = $row;
		}
		return $data;
	}

	function tambah($tambah_data){
		global $conn;

		$merek = htmlspecialchars($tambah_data['merek']);
		$type = htmlspecialchars($tambah_data['type']);
		$warna = htmlspecialchars($tambah_data['warna']);
		$harga = htmlspecialchars($tambah_data['harga']);

		//upload gambar

		$gambar = upload();
		if ( !$gambar ) {
			return false;
		}



		$sql = "INSERT INTO tbl_furniture VALUES
				('','$gambar','$merek','$type','$warna','$harga')
		";

		mysqli_query($conn, $sql);

		return mysqli_affected_rows($conn);

	}

	function upload(){
		$namaFile = $_FILES['gambar']['name'];
		$ukuranFile = $_FILES['gambar']['size'];
		$error = $_FILES['gambar']['error'];
		$tmpName = $_FILES['gambar']['tmp_name'];
		$path = 'img/';

		// cek apakah tidak ada gambar yang diupload

		if ( $error === 4 ) {
			echo "
				<script>
					alert('Pilih gambar terlebih dahulu !');
				</script>
			 ";
			 return false;
		}

		// cek extensi file yang boleh diupload

		$extensiGambarValid = ['jpg','jpeg','png'];
		$extensiGambar = explode('.', $namaFile);
		$extensiGambar = strtolower(end($extensiGambar));

		if ( !in_array($extensiGambar, $extensiGambarValid) ) {
			echo "
				<script>
					alert('Extensi file bukan gambar !');
				</script>
			 ";
			 return false;
		}

		// cek ukuran file

		if ($ukuranFile > 5000000) {
			echo "
				<script>
					alert('Ukuran gambar terlalu besar !');
				</script>
			 ";
			 return false;
		}

		// pemindahan file ke folder setelah lolos cek

		// antisipasi nama data duplikat

		$namaFileBaru = uniqid();
		$namaFileBaru .= '.';
		$namaFileBaru .= $extensiGambar;

		move_uploaded_file($tmpName, $path . $namaFileBaru);

		return $namaFileBaru;
	}

	function hapus($id){
		global $conn;

		mysqli_query($conn, "DELETE FROM tbl_furniture WHERE id=$id");
		return mysqli_affected_rows($conn);
	}

	function edit($hapus_data){
		 global $conn;

		$id = $_GET['id'];
		$merek = htmlspecialchars($hapus_data['merek']);
		$type = htmlspecialchars($hapus_data['type']);
		$warna = htmlspecialchars($hapus_data['warna']);
		$harga = htmlspecialchars($hapus_data['harga']);
		$gambar = htmlspecialchars($hapus_data['gambar']);

		$sql = "UPDATE tbl_furniture SET
			gambar =  '$gambar',
			merek = '$merek',
			type = '$type',
			warna = '$warna',
			harga = '$harga'

			WHERE id=$id
		";

		mysqli_query($conn, $sql);

		return mysqli_affected_rows($conn);

	}

	function cari($keyword){
		$sql = "SELECT * FROM tbl_furniture WHERE
				merek LIKE '%$keyword%' OR
				type LIKE '%$keyword%' OR
				warna LIKE '%$keyword%' OR
				harga LIKE '%$keyword%'
		";

		return read($sql);
	}
?>