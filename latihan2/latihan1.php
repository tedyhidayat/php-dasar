<?php
	// for ($i = 0; $i < 5; $i++) {
	// 	echo "Tedy Hidayat <br>";
	// }

	// $i=0;
	// while ($i < 5) {
	//     echo "Tedy Hidayat <br>";
	//  $i++;
	// }

	// $i=0;
	// do{
	// 	echo "Tedy Hidayat <br>";
	// $i++;
	// }while ($i < 5);
?>

<!DOCTYPE html>
<html>
<head>
	<title>Latihan php peerulangan</title>
	<style type="text/css" media="screen">
		.warna-1{
			background-color: gray;
			color: white;
		}
	</style>
</head>
<body>

<table border="1" cellpadding="10" cellspacing="0">
	<?php
		for ($baris = 1; $baris <= 5; $baris++) {
		 	echo "<tr>";
		 		for ($kolom = 1; $kolom <= 5; $kolom++) {
		 			echo "<td>$baris,$kolom</td>";
		 		}
		 	echo "</tr>";
		 }
	?>
</table>

<p>

<table border="1" cellspacing="0" cellpadding="10">
	<?php for ($baris = 1; $baris <= 5; $baris++) { ?>
			<tr>
				<?php for ($kolom = 1; $kolom <= 5 ; $kolom++) { ?>
					<td><?php echo"$baris, $kolom" ?></td>
				<?php } ?>
			</tr>
	<?php } ?>
</table>

<p>

<table border="1" cellspacing="0" cellpadding="10">
	<?php for ($baris = 1; $baris <= 5; $baris++) : ?>
			<tr>
				<?php for ($kolom = 1; $kolom <= 5 ; $kolom++) : ?>
					<td><?= "$baris, $kolom"; ?></td>
				<?php endfor; ?>
			</tr>
	<?php endfor; ?>
</table>

<p>

<table border="1" cellspacing="0" cellpadding="10">
	<?php for ($baris = 1; $baris <= 5; $baris++) : ?>
		<?php if($baris % 2 == 1) : ?>
			<tr class="warna-1">
		<?php else : ?>
			<tr>
		<?php endif; ?>
				<?php for ($kolom = 1; $kolom <= 5 ; $kolom++) : ?>
					<td><?= "$baris, $kolom"; ?></td>
				<?php endfor; ?>
			</tr>
	<?php endfor; ?>
</table>

</body>
</html>